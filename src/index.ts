import "./inputs/key-safety";

import RenderEngine from "./core/graphics/render-engine";
import Graphic from "./core/graphics/graphic";
import Vector from "./core/math/vector";

import Keyboard from "./inputs/keyboard";
import Mouse from "./inputs/mouse";

import GameMap, { MapName } from "./core/game-map";
import Player from "./actors/player";

const runTime = new RenderEngine(undefined);
const gameRenderer = new Graphic();
runTime.add(gameRenderer);

const gameWorld = new GameMap(gameRenderer);
gameWorld.loadMap(MapName.DE_TEST);

const input: any = {
	kb: new Keyboard(document),
	ms: new Mouse(document, gameRenderer),
};

const mouseGraphic = new Graphic();
gameRenderer.add(mouseGraphic);

const player = new Player(new Vector(), input.kb, input.ms);
gameRenderer.add(player);

const tick = () => {
	player.checkInputs();
	player.updateVelocity();
	player.updatePosition();
	mouseGraphic.offset = input.ms.offset;
	gameRenderer.offset = Vector.reverse(player.offset);
	requestAnimationFrame(tick);
};
tick();

// const world = new GameMap();
// runTime.add(world);
// world.loadMap('de_dust');

// canvas.loadMap(new TestMap());

// const gameLoop = () => {
// 	canvas.update();
// 	requestAnimationFrame(gameLoop);
// };

// gameLoop();
