import Graphic from "../core/graphics/graphic";
import Vector from "../core/math/vector";

type IVec = [ number, number ];
type Area = [ IVec, IVec ];

export default class Zone extends Graphic {
	private rect: [ Vector, Vector, Vector, Vector ];

	constructor(zone: { area: Area, name: string }) {
		super();

		this.rect = [
			new Vector(zone.area[ 0 ][ 0 ], zone.area[ 0 ][ 1 ]),
			new Vector(zone.area[ 0 ][ 1 ], zone.area[ 1 ][ 1 ]),
			new Vector(zone.area[ 1 ][ 1 ], zone.area[ 1 ][ 0 ]),
			new Vector(zone.area[ 1 ][ 0 ], zone.area[ 0 ][ 0 ])
		];
	}

	render() {
		this.fill = '#03b50036';
		this.stroke = '#03b5008a';

		this.renderRect(...this.rect);
	}
}