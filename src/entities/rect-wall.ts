import Wall from "./wall";
import Vector from "../core/math/vector";

export default class RectWall extends Wall {

	area: [ [ number, number ], [ number, number ] ];

	constructor(area: [ [ number, number ], [ number, number ] ], angle: number) {
		super(new Vector(...area[ 0 ]));
	}

	render(): void {
		this.colours();
		this.renderArea(this.area);
	}
}