import Graphic from "../core/graphics/graphic";

export default abstract class Wall extends Graphic {
	colours(): void {
		this.stroke = 'red';
		this.fill = '#6a6c61';
	}
}