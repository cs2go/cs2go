export default {
	size: [
		1000,
		1000
	],
	mode: 'defuse',
	bgimg: {
		file: 'de_dust.jpg',
		area: [
			[0, 0],
			[7000, 7000]
		]
	},

	spawns: [
		{
			name: 'T',
			area: [
				[4220, 1390],
				[4606, 2046]
			]
		},
		{
			name: 'CT',
			area: [
				[4220, 1390],
				[4606, 2046]
			]
		}
	],

	sites: [
		{
			name: 'A',
			area: [
				[5420, 1260],
				[5770, 1650]
			]
		}
	],

	walls: [
		//	LEFT A SHORT
		{
			type: 'round',
			pos: [4105, 2110],
			size: 130
		},
		{
			type: 'round',
			pos: [4105, 2110],
			size: 130
		},
		{
			type: 'rect',
			area: [
				[4180, 1117],
				[4190, 1117],
				[4190, 2715],
				[4180, 2715]
			]
		},

		// TOP CAT
		{
			type: 'round',
			pos: [4050, 2730],
			size: 140
		},
		{
			type: 'round',
			pos: [3445, 2730],
			size: 140
		},
		{
			type: 'rect',
			area: [
				[3550, 2720],
				[3940, 2720],
				[3940, 2730],
				[3550, 2730]
			]
		},

		// BOTTOM CAT
		{
			type: 'rect',
			area: [
				[3740, 3140],
				[4556, 3140],
				[4556, 3150],
				[3740, 3150]
			]
		},

		//	LEFT CATWALK
		{
			type: 'rect',
			area: [
				[3410, 2845],
				[3430, 2845],
				[3430, 4047],
				[3410, 4047]
			]
		},

		//	RIGHT CATWALK
		{
			type: 'round',
			pos: [3740, 3280],
			size: 140
		},
		{
			type: 'rect',
			area: [
				[3600, 3280],
				[3610, 3280],
				[3610, 4335],
				[3600, 4335]
			]
		},

		//	RIGHT PALM CORNER
		{
			type: 'round',
			pos: [3740, 4335],
			size: 140
		},

		//	RIGHT STAIRS
		{
			type: 'rect',
			area: [
				[4595, 2060],
				[4605, 2060],
				[4605, 3050],
				[4595, 3050]
			]
		},
		{
			type: 'rect',
			area: [
				[4595, 2060],
				[4820, 2060],
				[4820, 2075],
				[4595, 2075]
			]
		},

		//	RIGHT A SHORT
		{
			type: 'rect',
			area: [
				[4625, 1500],
				[4645, 1500],
				[4645, 2060],
				[4625, 2060]
			]
		},

		//	BOTTOM A PLAT 5394, 1670
		{
			type: 'rect',
			area: [
				[4625, 1500],
				[5415, 1500],
				[5415, 1520],
				[4625, 1520]
			]
		},
		{
			type: 'rect',
			area: [
				[5395, 1500],
				[5415, 1500],
				[5415, 1670],
				[5395, 1670]
			]
		},
		{
			type: 'rect',
			area: [
				[5395, 1650],
				[5790, 1650],
				[5790, 1670],
				[5395, 1670]
			]
		},

		// RIGHT A PLAT
		{
			type: 'rect',
			area: [
				[5770, 880],
				[5790, 880],
				[5790, 1670],
				[5770, 1670]
			]
		},

		//	TOP RIGHT NINJA
		{
			type: 'rect',
			area: [
				[4095, 860],
				[4105, 860],
				[4105, 980],
				[4095, 980]
			]
		},
		{
			type: 'rect',
			area: [
				[4095, 860],
				[4910, 860],
				[4910, 870],
				[4095, 870]
			]
		},

		//	TOP PIZZA
		{
			type: 'rect',
			area: [
				[4900, 870],
				[4910, 870],
				[4910, 950],
				[4900, 950]
			]
		},
		{
			type: 'rect',
			area: [
				[4900, 940],
				[5380, 940],
				[5380, 950],
				[4900, 950]
			]
		},
	],

	props: [
		//	ninja
		{
			type: 'crate',
			area: [
				[4710, 930],
				[170, 120]
			]
		},
		{
			type: 'crate',
			area: [
				[4860, 970],
				[90, 60]
			]
		},

		//	pizza
		{
			type: 'crate',
			area: [
				[5069, 969],
				[100, 50]
			]
		},

		//	A SITE
		//	Boxes
		{
			type: 'crate',
			area: [
				[5370, 1220],
				[100, 110]
			],
			angle: 15
		},
		{
			type: 'crate',
			area: [
				[5369, 1363],
				[100, 110]
			]
		},
		//	Safe
		{
			type: 'crate',
			area: [
				[5666, 1288],
				[100, 110]
			]
		},

		//	A SHORT
		{
			type: 'crate',
			area: [
				[4466, 2088],
				[120, 110]
			]
		},

		//	CAT BOOST
		{
			type: 'crate',
			area: [
				[4320, 3061],
				[225, 70]
			]
		},

		//	CAT
		{
			type: 'crate',
			area: [
				[3678, 2737],
				[150, 100]
			]
		},

		//	PALM
		{
			type: 'crate',
			area: [
				[3456, 4316],
				[60, 60]
			],
			angle: -35
		},
	]
}