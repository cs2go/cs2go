export default {
	size: [100, 100],
	mode: 'test',

	spawns: [
		{
			name: 'test',
			area: [
				[-50, -50],
				[50, 50]
			]
		}
	],

	walls: [
		{
			type: 'round',
			pos: [100, 100],
			size: 140
		},
		{
			type: 'rect',
			area: [
				[-100, -100],
				[20, 20]
			],
			angle: 35
		}
	]
}