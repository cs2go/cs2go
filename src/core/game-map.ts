import Vector from '../core/math/vector';
import Graphic from './graphics/graphic';
import Zone from '../entities/zone';


type IVector = [ number, number ];
type RectShape = [ IVector, IVector, IVector, IVector ];


export enum MapName { DE_DUST = 'de_dust', DE_TEST = 'de_test' }
enum SiteLabel { A, B }

enum EAffinity {
	T = 'terrorists',
	CT = 'counter-terrorists',
	H = 'hostage'
}
type Affinity = EAffinity | 'terrorists' | 'counter-terrorists' | 'hostage';


type RectWallShape = { type: 'rect', area: RectShape, angle?: number };
type RoundWallShape = { type: 'round', pos: IVector, size: number };
type WallShape = RectWallShape | RoundWallShape;

type ZoneShape = { name: Affinity, area: [ IVector, IVector ] }

type CrateShape = { type: 'crate', area: [ IVector, IVector ], angle?: number };


const mapValidator = (map: any) => {
	if (!map.size) throw new Error('MAP: requires a size');
	if (!map.mode) throw new Error('MAP: requires a mode');
	if (!map.spawns || !map.spawns.length) throw new Error('MAP: requires at least one spawn zone');
}


export default class GameMap {
	protected size: Vector = new Vector();

	protected spawnZones: Map<Affinity, Graphic> = new Map();
	protected bombSites: Map<SiteLabel, RectShape> = new Map();

	protected props: Set<Graphic> = new Set();

	protected container: Graphic;

	constructor(container: Graphic) {
		this.container = container;
	}

	async loadMap(map: MapName): Promise<void> {
		const _import = await import(`../maps/${ map }.js`);
		const mapData = _import.default;

		mapValidator(mapData);

		this.loadBGImage(mapData.bgimg);
		this.loadWalls(mapData.walls);
		this.loadProps(mapData.props);
		this.loadSites(mapData.sites);
		this.loadSpawns(mapData.spawns);
	}

	protected loadBGImage(walls: any): void {
		console.log('not implemented');
	}

	protected loadWalls(walls: WallShape[]): void {
		console.log('not implemented');
	}

	protected loadProps(props: any[]): void {
		console.log('not implemented');
	}

	protected loadSites(sites: ZoneShape[]): void {
		console.log('not implemented');
	}

	protected loadSpawns(zones: ZoneShape[]): void {
		zones.forEach((zone: ZoneShape) => {
			const _zone = new Zone(zone);
			this.container.add(_zone);
			this.spawnZones.set(zone.name, _zone);
		})
	}

	protected spawnPlayer(zone: ZoneShape): void { }
}


// const GameObject = require('./game-object');
// const Player = require('../players/human-player');

// const collision = require('../physics/collision');
// const Bullet = require('../weapons/bullet');

// const Point = require('../physics/point');
// const Line = require('../physics/line');
// const Ball = require('../physics/ball');

/*
module.exports = class GameMap extends GameObject {
	constructor() {
		super();
		Promise.resolve().then(() => GameMap.validateMap(this));


		this.debugMode = true;
		this.testPoints = [];
	}

	addTestPoints(...contactPoints) {
		contactPoints.forEach(contact =>
			this.testPoints.push({ contact, age: 50 })
		);
	}

	get offset() {
		if (!this.player) return super.offset;
		return Point.sub(super.offset, this.player.position);
	}

	//	shortcut the inherited root functions
	//	the map itself is the root.
	get root() {
		return this;
	}

	static validateMap(map) {
		if (!map.size) throw validMapErr('size');
		if (!map.spawn) throw validMapErr('spawn');
	}

	get width() { return this.size.x }
	get height() { return this.size.y }

	setParent(parentGraphic) {
		this.__parent = parentGraphic;
	}

	spawnPlayer() {
		let spwanPos = new Point(
			Math.random() * (this.spawn.end.x - this.spawn.start.x - 50) + (this.spawn.start.x + 25),
			Math.random() * (this.spawn.end.y - this.spawn.start.y - 50) + (this.spawn.start.y + 25)
		);

		this.player = new Player(spwanPos);
		this.add(this.player);
	}

	get walls() {
		return this.children.filter(child =>
			child instanceof Wall ||
			child instanceof RoundWall ||
			child instanceof Crate
		);
	}

	get bullets() {
		return this.children.filter(child => child instanceof Bullet);
	}

	_render() {
		super._render();

		if (this.debugMode) {
			this.stroke = 'red';
			this.fill = 'rgba(50, 50, 255, .3)';
			this.testPoints = this.testPoints
				.filter(p => this.debugMode)
				.filter(p => p.age > 0)
				.filter(p => !!p.contact);

			this.testPoints.forEach(p => {
				if (this.debugMode) this.renderPoint(p.contact);
				p.age -= 1;
			});
		}
	}

	__playerWallCollisions() {
		this.walls
			.filter(wall => collision.test(
				this.player.position,
				wall.position
			))
			.forEach(wall => {
				let points = [];

				if (wall instanceof Wall) {
					var hits = collision.rectBall(wall.position, this.player.position);
				}
				else if (wall instanceof RoundWall) {
					var hits = collision.ballBall(this.player.position, wall.position);
				}
				else if (wall instanceof Crate) {
					var hits = collision.rectBall(wall.position, this.player.position);
				}

				if (this.debugMode) {
					if (hits instanceof Array) this.addTestPoints(...hits);
					else this.addTestPoints(hits);
				}

				if (hits instanceof Array) points.push(...hits);
				else points.push(hits);


				if (points.length) {
					let contact = collision.getClosest(this.player.position, points);
					if (!contact) return;

					let relativeContact = contact.clone().sub(this.player.position);
					let limitTo = this.player.position.radius - relativeContact.length;

					this.player.position.sub(relativeContact.limit(limitTo));
					this.player.velocity = new Point();
				}
			});
	}

	__bulletWallCollisions() {
		this.bullets.forEach(bullet => {
			if (bullet.hasHit) {
				this.remove(bullet);
				return;
			}

			let points = [];

			this.walls
				.filter(wall => collision.test(
					bullet.asLine,
					wall.position
				))
				.forEach(wall => {
					if (wall instanceof Wall) {
						var hits = collision.rectLine(wall.position, bullet.asLine);
					}
					else if (wall instanceof RoundWall) {
						var hits = collision.lineBall(bullet.asLine, wall.position);
					}

					if (hits instanceof Array) points.push(...hits);
					else points.push(hits);
				});

			if (points.length) {
				let contact = collision.getClosest(bullet.startPos, points);
				if (!contact) return;

				bullet.hasHit = contact;
			}
		});
	}

	_update() {
		super._update();

		this.__playerWallCollisions();
		this.__bulletWallCollisions();
	}
}*/