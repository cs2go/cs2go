import { deg2rad, rad2deg } from './angles';

// export interface IVector {
// 	x: number;
// 	y: number;
// }

// export type IScalar = IVector | number;

export default class Vector {
	public x: number;
	public y: number;

	constructor(x: number = 0, y: number = 0) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Returns the distance from (0|0) -> (vec.x|vec.y)
	 */
	static magnitude(vec: Vector): number {
		return Math.sqrt(
			(vec.x * vec.x) +
			(vec.y * vec.y)
		);
	}

	/**
	 * Returns a new vector that is of magnitude 1, but maintains the angle.
	 */
	static normal(vec: Vector): Vector {
		const length: number = Vector.magnitude(vec);

		if (length === 0) return new Vector();

		return new Vector(
			vec.x == 0 ? 0 : vec.x / length,
			vec.y == 0 ? 0 : vec.y / length
		);
	}

	/**
	 * Returns the angle of the supplied vector (in degrees)
	 */
	static angle(vec: Vector): number {
		return rad2deg(Math.atan2(vec.x, vec.y));
	}

	/**
	 * Returns the distance between two Vectors.
	 */
	static distance(vec1: Vector, vec2: Vector): number {
		if (Vector.magnitude(vec1) > Vector.magnitude(vec2)) {
			return Vector.magnitude(Vector.sub(vec1, vec2));
		} else {
			return Vector.magnitude(Vector.sub(vec2, vec1));
		}
	}


	/**
	 * Returns a Vector which is the sum of the two supplied Vectors.
	 */
	static add(vec1: Vector, vec2: Vector): Vector {
		return new Vector(
			vec1.x + vec2.x,
			vec1.y + vec2.y
		);
	}

	/**
	 * Returns a Vector which is the sum of the first supplied Vector and the inverse if the second.
	 */
	static sub(vec1: Vector, vec2: Vector): Vector {
		return Vector.add(vec1, Vector.reverse(vec2))
	}

	/**
	 * Returns a Vector that is scaled by a factor of 'scalar' compared to the supplied Vector.
	 */
	static multiply(vec: Vector, scalar: number): Vector {
		return new Vector(
			vec.x * scalar,
			vec.y * scalar
		);
	}

	/**
	 * Returns a Vector that is the fraction of the the supplied Vector.
	 */
	static divide(vec: Vector, scalar: number): Vector {
		return new Vector(
			vec.x / scalar,
			vec.y / scalar
		);
	}


	/**
	 * Returns a Vector whos magnitude is the size of the limit supplied.
	 */
	static limit(vec: Vector, limit: number): Vector {
		return Vector.magnitude(vec) > limit ?
			Vector.multiply(Vector.normal(vec), limit) :
			Vector.clone(vec);
	}

	/**
	 * Returns a Vector that is rotated by 90deg anticlockwise to the supplied Vector.
	 */
	static left(vec: Vector): Vector {
		return new Vector(vec.x * -1, vec.y);
	}

	/**
	 * Returns a Vector that is rotated by 90deg clockwise to the supplied Vector.
	 */
	static right(vec: Vector): Vector {
		return new Vector(vec.x, vec.y * -1);
	}

	/**
	 * Returns a Vector that is rotated by 180deg from the supplied Vector.
	 */
	static reverse(vec: Vector): Vector {
		return Vector.multiply(vec, -1);
	}

	/**
	 * Returns a new Vector that maintains the same magnitude but is facing the direction provided.
	 */
	static rotateTo(vec: Vector, angle: number): Vector {
		const radians = deg2rad(Vector.angle(vec));

		const cos = Math.cos(radians);
		const sin = Math.cos(radians);

		return new Vector(
			(cos * vec.x) + (sin * vec.y),
			(cos * vec.y) - (sin * vec.x)
		)
	}

	/**
	 * Returns a new Vector that maintains the same magnitude but is rotated clockwise by the angle provided.
	 */
	static rotateBy(vec: Vector, angle: number): Vector {
		return Vector.rotateTo(vec, Vector.angle(vec) + angle);
	}

	/**
	 * Returns a duplicate Vector.
	 */
	static clone(vec: Vector): Vector {
		return new Vector(
			vec.x,
			vec.y
		);
	}

	/**
	 * Applies the values from the second vector to the first.
	 * @param subject the Vector to modify
	 * @param target the Vector to replicate
	 */
	static apply(subject: Vector, target: Vector): Vector {
		subject.x = target.x;
		subject.y = target.y;
		return subject;
	}
}