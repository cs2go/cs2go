import GraphicLayer from './graphic-layer';
import Vector from '../math/vector';

type IImage = {
	data: HTMLImageElement,
	hasLoaded: boolean
} | void;

const images: Map<string, IImage | void> = new Map();

export default class Graphic extends GraphicLayer {
	renderImg(url: string, x: number, y: number, w: number, h: number): void {
		if (images.has(url)) {
			const image: IImage = images.get(url);
			if (image && image.hasLoaded)
				this.ctx.drawImage(
					image.data,
					x + this.position.x,
					y + this.position.y,
					w, h
				)
		} else {
			const image: IImage = {
				data: new Image(),
				hasLoaded: false
			};

			image.data.src = url;
			image.data.onload = () => image.hasLoaded = true;
			images.set(url, image);
		}
	}

	renderPoint(point: Vector): void {
		this.renderBall(point, .5);
	}

	renderBall(point: Vector, radius: number): void {
		point = Vector.add(point, this.position);

		this.ctx.beginPath();
		this.ctx.arc(point.x, point.y, radius, 0, 360);
		this.ctx.stroke();
		this.ctx.fill();
	}

	renderLine(p1: Vector, p2: Vector): void {
		p1 = Vector.add(p1, this.position);
		p2 = Vector.add(p2, this.position);

		this.ctx.beginPath();
		this.ctx.moveTo(p1.x, p1.y);
		this.ctx.lineTo(p2.x, p2.y);
		this.ctx.closePath();
		this.ctx.stroke();
	}

	renderRect(p1: Vector, p2: Vector, p3: Vector, p4: Vector): void {
		[ p1, p2, p3, p4 ] = [ p1, p2, p3, p4 ].map(p => Vector.add(p, this.position));

		this.ctx.beginPath();

		this.ctx.moveTo(p1.x, p1.y);
		this.ctx.lineTo(p2.x, p2.y);
		this.ctx.lineTo(p3.x, p3.y);
		this.ctx.lineTo(p4.x, p4.y);
		this.ctx.lineTo(p1.x, p1.y);
		this.ctx.stroke();
		this.ctx.fill();
	}

	renderArea(area: [ [ number, number ], [ number, number ] ]): void {
		const vecs: [ Vector, Vector, Vector, Vector ] = [
			new Vector(area[ 0 ][ 0 ], area[ 0 ][ 1 ]),
			new Vector(area[ 0 ][ 1 ], area[ 1 ][ 0 ]),
			new Vector(area[ 1 ][ 0 ], area[ 1 ][ 1 ]),
			new Vector(area[ 1 ][ 1 ], area[ 1 ][ 0 ]),
		];

		//	rotate here

		this.renderRect(...vecs);
	}
}