import GraphicLayer from './graphic-layer';
import Vector from '../math/vector';

export const controllers: Map<HTMLCanvasElement, RenderEngine> = new Map();
export const getControllerFor = (canvas: HTMLCanvasElement) => controllers.get(canvas);

export default class RenderEngine extends GraphicLayer {
	protected _canvas?: HTMLCanvasElement;
	protected _rendering: Boolean = false;

	constructor(canvas: HTMLCanvasElement | void) {
		super();

		if (canvas) this._canvas = canvas;
		else {
			this._canvas = document.createElement('canvas');
			document.body.appendChild(this._canvas);
		}

		controllers.set(<HTMLCanvasElement>canvas, this);

		this._ctx = <CanvasRenderingContext2D>this._canvas.getContext('2d');

		window.addEventListener('resize', () => this.applyScreenSize());
		this.applyScreenSize();

		this._rendering = true;
		Promise.resolve().then(() => this._render());
	}

	protected _render(): void {
		if (this._rendering) {
			(<CanvasRenderingContext2D>this.ctx)
				.clearRect(
					0, 0,
					this.screenSize.x,
					this.screenSize.y
				);

			this._renderChildren();
		}

		requestAnimationFrame(() => this._render());
	}

	get canvasElement(): HTMLCanvasElement | void {
		return this._canvas;
	}

	private _screenSize?: Vector;
	get screenSize(): Vector {
		return Vector.clone(<Vector>this._screenSize);
	}

	get position(): Vector {
		return Vector.divide(this.screenSize, 2);
	}

	applyScreenSize(): void {
		this._screenSize = new Vector(
			window.innerWidth,
			window.innerHeight
		)

		this._canvas && this._canvas.setAttribute('width', this.screenSize.x + 'px');
		this._canvas && this._canvas.setAttribute('height', this.screenSize.y + 'px');
	}

	startRendering(): void {
		this._rendering = true;
	}

	stopRendering(): void {
		this._rendering = false;
	}
}