import Node from '../graph/node';
import Vector from '../math/vector';

export default abstract class GraphicLayer extends Node {
	offset: Vector = new Vector();
	get position(): Vector {
		return Vector.add(this.offset, (<GraphicLayer>this.parentNode).position);
	}

	private _fill: string = '';
	get fill() { return this._fill }
	set fill(fill: string) {
		this._fill = fill;
		this.ctx.fillStyle = fill;
	}

	private _stroke: string = '';
	get stroke() { return this._stroke }
	set stroke(stroke: string) {
		this._stroke = stroke;
		this.ctx.strokeStyle = stroke;
	}

	constructor(offset: Vector = new Vector()) {
		super();
		this.offset = offset;
	}

	protected _ctx?: CanvasRenderingContext2D;
	get ctx(): CanvasRenderingContext2D {
		if (this._ctx) return this._ctx;
		if (this.parentNode) return (<GraphicLayer>this.parentNode).ctx;
		throw new ReferenceError('There is no graphic context in this tree');
	}

	add(graphic: GraphicLayer): void {
		super.add(graphic);
	}

	public render?(): void;
	protected _render() {
		if (!this.ctx) return;

		if (typeof this.render == 'function') this.render();

		this._renderChildren();
	}

	protected _renderChildren() {
		(<GraphicLayer[]>this.children)
			.forEach((child: GraphicLayer) =>
				child._render()
			);
	}
}