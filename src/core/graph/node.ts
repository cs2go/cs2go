import Leaf from './leaf';

export default class Node extends Leaf {
	__children: Leaf[] = [];

	get children(): Leaf[] {
		return this.__children;
	}

	onChildAdded?(child: Leaf): void;
	onChildRemoved?(child: Leaf): void;

	add(child: Leaf): void {
		if (this.pathToNode.indexOf(child) > -1)
			throw new Error('Child node is an acestor to the parent node.');

		child.setParent(this);
		this.__children.push(child);

		if (this.onChildAdded)
			this.onChildAdded(<Leaf>child);
	}

	remove(child: Leaf): void {
		if (this.children.indexOf(child) > -1) {
			this.__children.splice(this.children.indexOf(child), 1);
		}
	}

	index(i: number): Leaf | undefined {
		return this.__children[ i ];
	}
}