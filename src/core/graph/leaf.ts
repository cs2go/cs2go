export default class Leaf {
	protected parent: Leaf | void = undefined;

	get root(): Leaf {
		return this.parentNode ?
			this.parentNode.root :
			this;
	}

	get parentNode(): Leaf | void {
		return this.parent;
	}

	get pathToNode() {
		let nodes: Leaf[] = [];
		let parent: Leaf = this;

		do {
			nodes.push(parent);

			if (parent.parent)
				parent = parent.parent;
		}

		while (parent.parent);

		return nodes;
	}


	remove?(child: Leaf): void;

	setParent(parent: Leaf) {
		if (this.parent && this.parent.remove)
			this.parent.remove(this);

		this.parent = parent;
	}
}