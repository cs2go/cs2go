console.warn('needs type updates');

export enum KeyMapping {
	up = 87,
	down = 83,
	left = 65,
	right = 68,
	shift = 16,
	ctrl = 17
}

export default class Keyboard {
	private keyStates: Map<number, boolean> = new Map();

	constructor(source: any) {
		source.addEventListener('keydown', (e: any) => this.keydown(e.keyCode));
		source.addEventListener('keyup', (e: any) => this.keyup(e.keyCode));

		source.onblur = () => {
			Array.from(this.keyStates.keys())
				.forEach(key => this.keyStates.set(key, false));
		}
	}

	isDown(button: KeyMapping): boolean {
		if (!this.keyStates.has(button)) return false;
		return <boolean>this.keyStates.get(button);
	}

	private keydown(key: KeyMapping): void {
		this.keyStates.set(key, true)
	}

	private keyup(key: KeyMapping): void {
		this.keyStates.set(key, false);
	}
}
