import Vector from '../core/math/vector';
import Graphic from '../core/graphics/graphic';

export enum ButtonMapping {
	primary,
	middle,
	second
}

console.warn('Needs type updates...');
export default class Mouse {
	protected state: Map<ButtonMapping, boolean> = new Map();
	public position: Vector = new Vector();

	private renderer: Graphic;

	constructor(source: any, renderer: Graphic) {
		this.renderer = renderer;

		source.addEventListener('mousemove', (e: any) => this.move(e));
		source.addEventListener('wheel', (e: any) => this.wheel(e));

		source.addEventListener('mousedown', (e: any) => this.mousedown(e.button));
		source.addEventListener('mouseup', (e: any) => this.mouseup(e.button));
	}

	isDown(button: ButtonMapping): Boolean {
		if (!this.state.has(button)) return false;
		return <Boolean>this.state.get(button);
	}

	get angle(): number {
		return Vector.angle(this.renderer.offset);
	}

	//	update types here pls.
	private move(e: any): void {
		this.position = new Vector(e.clientX, e.clientY);
	}

	private mousedown(button: ButtonMapping): void {
		this.state.set(button, true);
	}

	private mouseup(button: ButtonMapping): void {
		this.state.set(button, false);
	}

	private wheel(e: any): void {
		console.log('wheel change');
	}
}
