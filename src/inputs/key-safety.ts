const keyBlacklist: Number[] = [87, 68, 69, 83];

// window.onbeforeunload = (e: any) => {
// 	e.preventDefault();
// 	e.returnValue = 'Are you sure you want to leave?';
// }

window.addEventListener("keydown", (e: KeyboardEvent) => {
	if (!e.ctrlKey) return; //	dont worry about other key combos just yet

	console.log('CTRL:', e.keyCode, keyBlacklist);

	if (keyBlacklist.includes(e.keyCode)) {
		e.preventDefault();
		e.stopPropagation();
		return false;
	}
});
