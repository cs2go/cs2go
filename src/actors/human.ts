import Vector from "../core/math/vector";
import Graphic from "../core/graphics/graphic";

export enum Stance {
	running,
	walking,
	crouching,
}

export default abstract class Human extends Graphic {
	static friction: number = 0.9;
	static acceleration: number = 1.5;

	protected stance: Stance = Stance.running;
	static runSpeed: number = 4;
	static walkSpeed: number = 2.25;
	static crouchSpeed: number = 1.25;

	protected velocity: Vector = new Vector();
	protected angle: number = 0;

	get stanceSpeed(): number {
		if (this.stance == Stance.walking) return Human.walkSpeed;
		if (this.stance == Stance.crouching) return Human.crouchSpeed;
		return Human.runSpeed;
	}

	updateVelocity(): void {
		this.velocity = Vector.multiply(this.velocity, Human.friction);
		this.velocity = Vector.limit(this.velocity, this.stanceSpeed);
		if (Vector.magnitude(this.velocity) < .01) this.velocity = new Vector();
	}

	updatePosition(): void {
		this.offset = Vector.add(this.offset, this.velocity);
	}

	private fire(): void {
		console.log("*Pew*. You step back in shock.");
		console.log('"What? I thought I was out!? Game on bitches!');
	}

	protected pullTrigger(): void {
		if (!(<any>this).equiped) {
			console.log(
				"Sweat dripping down your face, you frantically pull the trigger.",
			);
			console.log("*Click*");
			console.log('"Shit! I\'m out! Fuck!"');
		}
	}

	abstract render(): void;
}
