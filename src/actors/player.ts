import Human, { Stance } from "./human";

import Keyboard, { KeyMapping } from '../inputs/keyboard';
import Mouse, { ButtonMapping } from '../inputs/mouse';
import Vector from "../core/math/vector";

export default class Player extends Human {
	private keyboard: Keyboard;
	private mouse: Mouse;

	constructor(position: Vector, keyboard: Keyboard, mouse: Mouse) {
		super(position);

		this.keyboard = keyboard;
		this.mouse = mouse;
	}

	checkInputs(): void {
		this.angle = this.mouse.angle;
		// this.angle = this.mouseOffset.angle - 90;

		//	stance
		if (this.keyboard.isDown(KeyMapping.ctrl)) this.stance = Stance.crouching;
		else if (this.keyboard.isDown(KeyMapping.shift)) this.stance = Stance.walking;
		else this.stance = Stance.running;


		if (this.mouse.isDown(ButtonMapping.primary)) this.pullTrigger();

		if (this.keyboard.isDown(KeyMapping.left))
			this.velocity = Vector.add(this.velocity, new Vector(-this.stanceSpeed, 0))

		if (this.keyboard.isDown(KeyMapping.right))
			this.velocity = Vector.add(this.velocity, new Vector(this.stanceSpeed, 0))

		if (this.keyboard.isDown(KeyMapping.up))
			this.velocity = Vector.add(this.velocity, new Vector(0, -this.stanceSpeed))

		if (this.keyboard.isDown(KeyMapping.down))
			this.velocity = Vector.add(this.velocity, new Vector(0, this.stanceSpeed))
	}

	render() {
		this.fill = "#003869e3";
		this.stroke = "#001b33";
		this.renderBall(new Vector(), 25);

		const mouseOffset =

			this.renderLine(new Vector(), Vector.sub(Vector.reverse(this.mouse.position), this.position));
	}
}