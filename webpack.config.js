const path = require('path');

module.exports = {
	entry: './src/index.ts',
	mode: 'development',
	devServer: {
		contentBase: path.join(__dirname, 'public'),
		compress: true,
		port: 8080
	},
	module: {
		rules: [
			{
				test: /\.ts/,
				use: 'ts-loader',
				exclude: /node_modules/,
			}
		]
	},
	resolve: {
		modules: ['src', 'node_modules'],
		extensions: ['.ts', '.js']
	},
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'public')
	}
}